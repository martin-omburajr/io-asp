﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Io_ASP.NET.Startup))]
namespace Io_ASP.NET
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
